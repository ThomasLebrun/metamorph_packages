import subprocess
import requests
import os
import time

class WebTrafficGenerator:
    def __init__(self):
        self.urls = [
            "https://www.google.com",
            "https://www.wikipedia.org",
            "https://www.python.org",
            # Ajoutez d'autres URLs que vous voulez ouvrir ici
        ]

        self.browsers_paths = {
            "firefox": "C:/Program Files/Mozilla Firefox/firefox.exe",
            "chrome": "C:/Program Files/Google/Chrome/Application/chrome.exe",
            "edge": "C:/Program Files (x86)/Microsoft/Edge/Application/msedge.exe"
        }

    def open_urls(self):
        for _, browsers in self.browsers_paths.items():
            if os.path.exists(browsers):
                for url in self.urls:
                    subprocess.Popen([browsers, url])

if __name__ == "__main__":
    WebTrafficGenerator().open_urls()
    time.sleep(30)